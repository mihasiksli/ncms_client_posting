ncmsApp
    .factory('PostingWallService', function($rootScope, $resource, ConfigService) {
        return $resource(
            ConfigService.CORE_API_HOST + '/api/v1/posting/walls/:wallId/',
            {wallId: '@id'}
        )
    })
    .factory('PostingWallFindService', function($rootScope, $resource, ConfigService){
        return $resource(
            ConfigService.CORE_API_HOST + '/api/v1/posting/walls/find/'
        )

    })
    .factory('FileSaveConfigService', function() {
        return {
            save: {
                    method: 'POST',
                    headers: {"Content-Type": undefined},
                    transformRequest: function(data) {
                        var fd = new FormData();
                        angular.forEach(data, function(v, k) {
                            fd.append(k, v);
                        });
                        return fd;
                    }
            }
        }
    })
    .factory('FileListService', function($rootScope) {
        return {fileList: []};
    })
    .factory('PostService', function($rootScope, $resource, ConfigService) {
        return $resource(
            ConfigService.CORE_API_HOST + '/api/v1/posting/posts/:postId/',
            {postId: '@id'},
            {
                results: {method: 'GET', params: {page: '@page'}},
                create: {method: 'POST'},
                update: {method: 'PUT', params: {postId: '@id'}}
            }
        );
    })
    .factory('PostImageService', function ($rootScope, $resource, ConfigService, FileSaveConfigService) {
        return $resource(
            ConfigService.CORE_API_HOST + '/api/v1/posting/images/:imageId/',
            {imageId: '@id'},
            {
                save: FileSaveConfigService.save,
                updateOrder: {method: 'PATCH', params: {imageId: '@id'}}
            }
        )
    })
    .factory('PostPollService', function($rootScope, $resource, ConfigService) {
        return $resource(
            ConfigService.CORE_API_HOST + '/api/v1/posting/polls/:pollId/',
            {pollId: '@id'}
        )
    })
    .factory('PostPollAnswerService', function($rootScope, $resource, ConfigService) {
        return $resource(
            ConfigService.CORE_API_HOST + '/api/v1/posting/pollanswers/:pollAnswerId/',
            {pollAnswerId: '@id'}
        )
    })
    .factory('PostDocService', function($rootScope, $resource, ConfigService, FileSaveConfigService) {
        return $resource(
            ConfigService.CORE_API_HOST + '/api/v1/posting/docs/:docId/',
            {docId: '@id'},
            {save: FileSaveConfigService.save}
        )
    });
