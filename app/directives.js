ncmsApp
    .directive('fileListInput', ['$parse', 'FileListService', function($parse, FileListService) {
        return {
            restrict: 'A',
            scope: {},
            link: function(scope, element, attrs) {
                element.bind('change', function(event) {
                    scope.$emit('fileListChanged', {'fileList': event.target.files});
                    //FileListService.fileList = event.target.files;
                });
            },
            templateUrl: '/views/posting/file-list-input.html'
        }
    }])
    .directive('postingDropContainer', ['$parse', function($parse) {
        return {
            restrict: 'E',
            scope: {},
            link: function(scope, element, attrs) {
                processDragOverOrEnter = function(event) {
                    if (event != null)
                        event.preventDefault();
                    attrs.class = "";
                    return false;
                };
                element.bind('dragover', processDragOverOrEnter);
                element.bind('dragenter', processDragOverOrEnter);
                element.bind('drop', function(event) {
                    if (event != null)
                        event.preventDefault();
                    scope.$emit('fileListChanged', {'fileList': event.dataTransfer.files});
                    return false;
                });
            },
            templateUrl: '/views/posting/drop-container.html'
        }
    }])
    .directive('postingPollContainer', ['$parse', function($parse) {
        return {
            restrict: 'E',
            scope: {},
            link: function(scope, element, attrs) {

            },
            templateUrl: '/views/posting/poll-create-container.html'
        }
    }])
    .directive('sortableImageContainer', ['$parse', function($parse) {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                Sortable.create(element[0], {
                    draggable: ".draggable-image-container",
                    onUpdate: function(e) {
                        scope.changeImageOrder(e.oldIndex, e.newIndex);
                    }
                });
            }
        }
    }]);
