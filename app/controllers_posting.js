'use strict';

ncmsApp
    .controller('PostingWallsCtrl', function($scope, $rootScope, $location,
                                             PostingWallService, PostingWallFindService) {
        PostingWallService.query().$promise.then(function(d) {
            $scope.walls = d;
        });
        $scope.loadWalls = function() {
            PostingWallService.query().$promise.then(function(d) {
                $scope.walls = d;
            });
        };
        $scope.findWalls = function() {
            $scope.wallSearching = true;
            $scope.newWalls = [];
            PostingWallFindService.query().$promise.then(function(d) {
                $scope.newWalls = d;
                $scope.wallSearching = false;
            }).catch(function(d) {$scope.wallSearching = false;});
        };
        $scope.saveWall = function(nw) {
            PostingWallService.save(nw).$promise.then(function(d) {
                $scope.loadWalls();
            });
        };
        $scope.saveWalls = function() {
            var wallsToSave = [];
            angular.forEach($scope.newWalls, function(nw) {
                if (nw.selected) wallsToSave.push(nw);
            });
            $scope.newWalls = [];
            angular.forEach(wallsToSave, function(w) {
                $scope.saveWall(w);
            });
        };
        $scope.deleteWall = function(id) {
            PostingWallService.delete({wallId: id}).$promise.then(function(d) {
                $scope.loadWalls();
            });
        };
        $scope.toggle = function(item) { item.selected = !item.selected; };
        $scope.anySelected = function() {
            var any = false;
            angular.forEach($scope.newWalls, function(nw) {
                if (nw.selected) any = true;
            });
            return any;
        };
        $scope.newWalls = [];
        $scope.wallSearching = false;
        $scope.loadWalls();
    })
    .controller('PostingCtrl', function($scope, $rootScope, $location, PostService) {
        PostService.results({page: 1}).$promise.then(function(d) {
            $scope.posts = $scope.processPosts(d.results);
        });
        $scope.loadPost = function(postId) {
            $location.path('/posting/post/' + postId);
        };
        $scope.processPost = function(post) {
            post.created_at = new Date(post.created_at);
            return post;
        };
        $scope.processPosts = function(postList) {
            angular.forEach(postList, function(element) {$scope.processPost(element);});
            return postList;
        };
        $scope.createPost = function() {
            if (!window.confirm('Create post?')) return;
            PostService.create().$promise.then(function(d) {
                $location.path('/posting/post/' + d.id);
            }).catch(function(data) { $location.path('/posting'); });
        };
    })
    .filter('reformatTimeNumber', function() {
        return function(n) {
            if (n.toString().length == 1) return '0' + n.toString();
            return n.toString();
        }
    })
    .controller('PostCtrl', function($scope, $rootScope, $location, $routeParams, PostService,
                                     PostImageService, PostDocService, PostPollService,
                                     PostingWallService) {
        $scope.postValid = true;
        Array.prototype.move = function(from, to) {
            this.splice(to, 0, this.splice(from, 1)[0]);
        };
        $scope.loadPost = function() {
            PostService.get({postId: $routeParams.postId}).$promise.then(function(d) {
                $scope.post = d;
                if ($scope.post.albums.length > 0) {
                    $scope.imageList = $scope.post.albums[0].images;
                }
                else {
                    $scope.imageList = $scope.post.images;
                }
            }).catch(function() {
                $location.path('/posting')
            });
        };
        $scope.updatePost = function() {
            if ($scope.post) {
                PostService.update({postId: $scope.post.id}, $scope.post, function(d) {
                    $scope.post = d;
                });
            }
        };
        $scope.closePost = function() {
            $location.path('/posting');
        };
        $scope.deletePost = function() {
            if (window.confirm('Delete post')) {
                PostService.delete({postId: $scope.post.id}).$promise.then(function() {
                    $location.path('/posting');
                });
            }
        };
        $scope.uploadFile = function(f, index) {
            if (f === undefined || !$scope.checkFileValid(f)) return;
            var d;
            var postFileService;
            switch ($scope.selectedOption.type) {
                case 'image':
                    d = {'image': f, 'order': index};
                    var album = $scope.post.albums[0];
                    if (album == undefined) {
                        d['post'] = $scope.post.id;
                    }
                    else { d['album'] = album.id }
                    postFileService = PostImageService;
                    break;
                case 'doc':
                    d = {'post': $scope.post.id, 'file': f, 'name': f.name};
                    postFileService = PostDocService;
                    break;
                default:
                    return;
            }
            postFileService.save(d, function(r) {$scope.loadPost(); });
        };
        $scope.deleteFile = function(id, fileType) {
            var d, delService;
            switch (fileType) {
                case 'image':
                    delService = PostImageService;
                    d = {'imageId': id};
                    break;
                case 'doc':
                    delService = PostDocService;
                    d = {'docId': id};
                    break;
                default:
                    return;
            }
            delService.delete(d).$promise.then(function() {$scope.loadPost();});
        };
        $scope.checkFileValid = function(f) {
            if ($scope.selectedOption == undefined) return false;
            if ($scope.selectedOption.type == 'image') {
                return f.type.indexOf('image') > -1;
            }
            else if ($scope.selectedOption.type == 'doc') {
                return true;
            }
            return false;
        };
        $scope.updateImageOrder = function(image) {
            PostImageService.updateOrder(
                {imageId: image.id},
                {'id': image.id, 'order': image.order},
                function() {$scope.loadPost();}
            );
        };
        $scope.changeImageOrder = function(oldPos, newPos) {
            if (newPos < 0 || newPos >= $scope.imageList.length) return;
            $scope.imageList.move(oldPos, newPos);
            angular.forEach($scope.imageList, function(i, index) {
                i.order = index;
                $scope.updateImageOrder(i);
            });
        };
        $scope.checkPollAnswersValid = function() {
            angular.forEach($scope.newPoll.answers, function() {

            });
            return false;
        };
        $scope.makeValidArray = function(arr) {
            var newArr = [];
            angular.forEach(arr, function(el) {
                if (el && el != "") newArr.push(el);
            });
            return newArr
        };
        $scope.savePoll = function() {
            $scope.newPoll.answers = $scope.makeValidArray($scope.newPoll.answers);
            if ($scope.newPoll.title == undefined || $scope.newPoll.title === '' ||
                $scope.newPoll.answers.length == 0
            ) console.log('nah');
            $scope.newPoll.post = $scope.post.id;
            PostPollService.save($scope.newPoll).$promise.then(function() {$scope.loadPost();});
        };
        $scope.deletePoll = function() {
            if ($scope.post.poll.length < 1) return;
            PostPollService.delete({'pollId': $scope.post.poll[0].id}).$promise.then(function() {
                $scope.newPoll = {};
                $scope.loadPost();
            });
        };

        $scope.attachmentOptions = [{type: 'image', 'name': 'image'},
                                    {type: 'doc', 'name': 'doc'},
                                    {type: 'poll', 'name': 'poll'}
        ];
        $scope.selectedOptionIndex = 0;
        $scope.newPoll = {};

        $scope.$watch('newPoll.title', function(d) {
            if ($scope.newPoll.title && $scope.newPoll.answers == undefined) {
                $scope.newPoll.answers = [""];
            }
        });
        $scope.$watchCollection('newPoll.answers', function(d) {
            if ($scope.newPoll.answers == undefined) return;
            if ($scope.newPoll.answers[$scope.newPoll.answers.length - 1] != "") $scope.newPoll.answers.push("");
        });
        $scope.removePollAnswer = function(aIndex) {
            $scope.newPoll.answers.splice(aIndex, 1);
        };

        $scope.$watch('post.text', function(d) {$scope.updatePost();});
        $scope.$watch('post.link', function(d) {$scope.updatePost()});
        $scope.$watch('selectedOptionIndex', function(d) {
            $scope.selectedOption = $scope.attachmentOptions[$scope.selectedOptionIndex];
        });
        $scope.$on('fileListChanged', function(event, args) {
            if ($scope.post == undefined) return;
            var imagesOrders = $scope.imageList.map(function(image) {return image.order});
            var nextOrder = imagesOrders.length > 0 ? Math.max.apply( Math, imagesOrders ) + 1 : 0;
            angular.forEach(args.fileList, function(f, index) {
                $scope.uploadFile(f, index + nextOrder);
            });
        });

        $scope.$watch('attachmentOptionSelected', function(d) {});
        $scope.toggleWallSelected = function(wall) {
            wall.selected = !wall.selected;
        };
        $scope.selectedWallList = function() {
            var wallsSelected = [];
            angular.forEach($scope.walls, function(w) {
                if (w.selected) wallsSelected.push(w.id);
            });
            return wallsSelected;
        };
        $scope.publishPost = function() {
           var publishData = {'post': $scope.post.id, walls: $scope.selectedWallList()};
            if ($scope.publishTime) publishData.time = $scope.publishTime;
            console.log($scope.hours + '  ' + $scope.minutes);

        };
        $scope.deleteTime = function() {
            $scope.publishTime = undefined;
        };
        $scope.hours = 0;
        $scope.minutes = 0;
        $scope.hourList = [];
        $scope.minuteList = [];

        for (var i = 0; i < 24; i++) $scope.hourList.push(i);
        for (i = 0; i < 60; i++) $scope.minuteList.push(i);

        PostingWallService.query().$promise.then(function(d) {
            $scope.walls = d;
        }).catch(function() {$scope.walls = []});
        $scope.loadPost();
    });
