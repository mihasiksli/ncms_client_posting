ncmsApp
    .controller('TranslationsCtrl', function ($scope, $rootScope, $localStorage, TranslationsService) {
        $scope.lang = $localStorage.lang || 'en';
        $rootScope.$watch('user', function (data) {
            if (data) {
                $scope.lang = data.lang || 'en';
                $localStorage.lang = $scope.lang;
            }
        });
        TranslationsService.query().$promise.then(function (data) {
            $scope.trans = function (slug, lang) {
                lang = lang || $scope.lang;
                try {
                    return data[slug]['text_' + lang];
                } catch (error) {
                    console.log(error);
                    return slug;
                }
            };
        });
        $scope.updateGlobalLang = function (lang) {
            $localStorage.lang = lang;
        }
    })
    .controller('AuthCtrl', function ($scope, $rootScope, $location, UserService) {
        $scope.route = function (path) {
            $location.path(path);
        };
        $scope.VKLogin = function () {
            UserService.VKLogin().then(function (data) {
                $rootScope.user = data;
            }).catch(function (data) {
                console.log(data);
                alert('Error. See console.')
            });
        };
        $scope.FBLogin = function () {
            UserService.FBLogin().then(function (data) {
                $rootScope.user = data;
            }).catch(function (data) {
                console.log(data);
                alert('Error. See console.')
            });
        };
        $scope.GPlusLogin = function () {
            UserService.GPlusLogin().then(function (data) {
                $scope.user = data;
            }).catch(function (data) {
                console.log(data);
                alert('Error. See console.')
            });
        };
        $scope.emailLogin = function (email, password) {
            UserService.emailLogin(email, password).then(function (data) {
                $rootScope.user = data;
            }).catch(function (data) {
                console.log(data);
                alert('Error. See console.')
            });
        };
        $scope.logout = function () {
            UserService.logout().catch(function (data) {
                console.log(data);
                alert('Error. See console.')
            })
        }
    })
    .controller('SidebarCtrl', function($scope) {})
    .controller('RightSidebarCtrl', function($scope) {})
    .controller('MainCtrl', function($scope, $localStorage) {
        console.log($localStorage.user.token);
    })
;
