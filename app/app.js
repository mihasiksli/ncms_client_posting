'use strict';

var ncmsApp = angular.module('ncms', ['ngMaterial', 'ngRoute', 'ngResource', 'ngCookies', 'ngStorage',
    'ngSanitize', 'ngMessages', 'angular-inview', 'ADM-dateTimePicker']);

ncmsApp
    .factory('httpRequestInterceptor', ['$cookies', '$localStorage', function($cookies, $localStorage) {
        return {
            request: function(config) {
                if ($localStorage.user) {
                    config.headers['Authorization'] = 'Token ' + $localStorage.user.token;
                }
                return config;
            }
        }
    }])
    .config(['$routeProvider', '$resourceProvider', '$httpProvider', '$mdThemingProvider',
        function($routeProvider, $resourceProvider, $httpProvider, $mdThemingProvider) {
            $routeProvider.when('/', {
                templateUrl: '/views/main.html',
                controller: 'MainCtrl as mainCtrl'
            }).when('/login', {
                templateUrl: '/views/login.html',
                controller: 'AuthCtrl'

            }).when('/posting', {
                templateUrl: '/views/posting/posting.html',
                controller: 'PostingCtrl'
            }).when('/posting/post/:postId', {
                templateUrl: 'views/posting/post.html',
                controller: 'PostCtrl'
            }).when('/posting/walls/', {
                templateUrl: 'views/posting/walls.html',
                controller: 'PostingWallsCtrl'
            }).otherwise({
                redirectTo: '/'
            });

            $httpProvider.interceptors.push('httpRequestInterceptor');
            $resourceProvider.defaults.stripTrailingSlashes = false;

            var customTheme = $mdThemingProvider.extendPalette('cyan', {
                '800': '#00BFA5'
            });

            $mdThemingProvider.definePalette('custom-cyan', customTheme);

            $mdThemingProvider
                .theme('default')
                .primaryPalette('deep-purple', {
                    default: '800'
                })
                .warnPalette('red', {
                    default: 'A400'
                })
                .accentPalette('custom-cyan', {
                    'default': '800'
                });
        }
    ])
    .run(function ($window, $rootScope, $location, $localStorage, $http, ConfigService) {
        if (configData == undefined) {
            alert('Config loading error')
        }
        $rootScope.config = ConfigService;
        // check user login
        var postLogInRoute;
        $rootScope.user = $localStorage.user;
        $rootScope.$on("$routeChangeStart", function (event, next, current) {
            if (!postLogInRoute) {
                if ($location.path() != '/login' && $location.path() != '/registration') {
                    postLogInRoute = $location.path();
                } else {
                    postLogInRoute = '/';
                }
            }
            if (!$rootScope.user) {
                if (next.templateUrl != "/views/login.html" && next.templateUrl != "/views/registration.html" ) {
                    $location.path("/login").replace();
                }
            }
        });

        $rootScope.$on('loginEvent', function () {
            if (postLogInRoute) {
                $rootScope.$apply(function () {
                    $location.path(postLogInRoute).replace();
                    postLogInRoute = null;
                })
            }
        });

        $rootScope.$on('logoutEvent', function () {
            $location.path('/login').replace();
            postLogInRoute = null;
        });
    });
