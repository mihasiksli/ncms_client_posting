ncmsApp
    .factory('ConfigService', function() {
        return configData;
    })
    .factory('UserService', function ($rootScope, $http, $localStorage, $q, $timeout, $window, ConfigService) {
        var config = ConfigService;
        // VK INIT
        VK.init({apiId: config.VK_APP_ID});
        // FB INIT
        $window.fbAsyncInit = function () {
            FB.init({
                appId: config.FB_APP_ID,
                xfbml: true,
                version: 'v2.5'
            });
        };
        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {
                return;
            }
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));

        $rootScope.$watch('user', function () {
            $localStorage.user = $rootScope.user;
        });

        return {
            user: function () {
                return $rootScope.user;
            },
            getMe: function () {
                return $http.get(config.CORE_API_HOST + '/api/v1/users/me/').then(function (response) {
                    return response.data;
                })
            },
            VKLogin: function () {
                var deferred = $q.defer();

                VK.Auth.login(function (response) {
                    var postData = response.session;
                    postData.social = 'vk';
                    var userData = {};
                    $http.post(config.CORE_API_HOST + '/api/v1/login/', postData).success(function (data) {
                        if (data.status) {
                            userData = data.data;
                            VK.api('users.get', {
                                access_token: response.session.sid,
                                fields: 'photo_100'
                            }, function (data) {
                                userData.avatar = data.response[0].photo_100;
                                userData.name = data.response[0].first_name + ' ' + data.response[0].last_name;
                                $rootScope.$apply(
                                    $rootScope.user = userData, $localStorage.user = $rootScope.user, $rootScope.$broadcast('loginEvent')
                                );
                                deferred.resolve(userData);
                            })
                        }
                    }).catch(function (data) {
                        deferred.reject(data);
                    })
                });

                return deferred.promise;
            },
            FBLogin: function () {
                var deferred = $q.defer();

                FB.login(function (response) {
                    if (response.status === 'connected') {
                        var postData = {
                            "social": "fb",
                            "uid": response.authResponse.userID,
                            "token": response.authResponse.accessToken
                        };
                        var userData = {};

                        $http.post(config.CORE_API_HOST + "/api/v1/login/", postData).success(function (data) {
                            if (data.status) {
                                userData = data.data;
                                if (FB) {
                                    FB.api("/me", {fields: ['id', 'name', 'picture']},
                                        function (response) {
                                            if (response && !response.error) {
                                                userData.avatar = response.picture.data.url;
                                                userData.name = response.name;
                                                $rootScope.$apply(
                                                    $rootScope.user = userData, $localStorage.user = $rootScope.user, $rootScope.$broadcast('loginEvent')
                                                );
                                                deferred.resolve(userData);
                                            }
                                        }
                                    );
                                }
                            }
                        }).catch(function (data) {
                            deferred.reject(data);
                        });
                    }
                });

                return deferred.promise;
            },
            GPlusLogin: function () {
                var deferred = $q.defer();
                gapi.load('auth2', function() {
                    var auth = gapi.auth2.init({
                        client_id: config.GPLUS_APP_ID,
                        cookiepolicy: 'single_host_origin',
                        scope: 'profile email'
                    });
                    auth.signIn().then(function(d) {
                        var postData = d.getAuthResponse();
                        postData.social = 'google';
                        $http.post(config.CORE_API_HOST + "/api/v1/login/", postData).success(function (data) {
                            if (data.status) {
                                var userData = data.data;
                                $http.get('https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=' + postData.id_token).then(function (data) {
                                    userData.first_name = data.data.given_name;
                                    userData.last_name = data.data.family_name;
                                    userData.avatar = data.data.picture;
                                    userData.name = data.data.name;
                                    $rootScope.user = userData, $localStorage.user = $rootScope.user;
                                    $timeout(function () {
                                        $rootScope.$broadcast('loginEvent');
                                    });
                                    deferred.resolve(userData);
                                }).catch(function (response) {
                                    deferred.reject()
                                });
                            } else {
                                deferred.reject();
                            }
                        });
                    });
                });
                return deferred.promise;
            },
            emailLogin: function (email, password) {
                return $http.post(config.CORE_API_HOST + '/api/v1/login/', {
                    email: email,
                    password: password
                }).then(function (response) {
                    var data = response.data;
                    $rootScope.user = data.data;
                    $localStorage.user = $rootScope.user;
                    $timeout(function () {
                        $rootScope.$broadcast('loginEvent');
                    }, 0);
                    return data.data;
                })
            },
            emailRegistration: function (email, password) {
                return $http.post(config.CORE_API_HOST + '/api/v1/registration/', {
                    email: email,
                    password: password
                }).then(function (response) {
                    var data = response.data;
                    return data.data;
                })
            },
            emailConfirmation: function (email, code) {
                return $http.post(config.CORE_API_HOST + '/api/v1/registration/confirm/', {
                    email: email,
                    code: code
                }).then(function (response) {
                    var data = response.data;
                    $rootScope.user = data.data;
                    $localStorage.user = $rootScope.user;
                    $timeout(function () {
                        $rootScope.$broadcast('loginEvent');
                    }, 0);
                    return data.data;
                })
            },
            logout: function () {
                return $http.post(config.CORE_API_HOST + '/api/v1/logout/', {}).then(function (response) {
                    var data = response.data;
                    $rootScope.user = null;
                    $localStorage.user = null;
                    $rootScope.$broadcast('logoutEvent');
                    return data;
                })
            },
            changeEmail: function (email) {
                return $http.patch(config.CORE_API_HOST + '/api/v1/users/me/update/', {email: email}).then(function (response) {
                    $rootScope.user = response.data;
                    return response.data;
                })
            },
            changeData: function (data) {
                return $http.patch(config.CORE_API_HOST + '/api/v1/users/me/update/', data).then(function (response) {
                    $rootScope.user = response.data;
                    return response.data;
                })
            },
            changePassword: function (oldPassword, newPassword) {
                return $http.post(
                    config.CORE_API_HOST + '/api/v1/users/change-password/',
                    {old_password: oldPassword, new_password: newPassword}
                );
            },
            updateTimezone: function (tz) {
                return $http.patch(
                    config.CORE_API_HOST + '/api/v1/users/me/update/',
                    {tz: tz}
                ).then(function () {
                    $rootScope.user.tz = tz;
                    $localStorage.user.tz = tz;
                })
            },
            updateLang: function (lang) {
                return $http.patch(
                    config.CORE_API_HOST + '/api/v1/users/me/update/',
                    {lang: lang}
                ).then(function () {
                    $rootScope.user.lang = lang;
                    $localStorage.user.lang = lang;
                })
            }
        }
    })
    .factory('DialogService', function ($rootScope, $mdDialog) {

    })
    .factory('SourceService', function($rootScope, $resource, ConfigService) {
        return $resource(ConfigService.CORE_API_HOST + '/api/v1/sources/:sourceId/', {sourceId: '@id'});
    })
    .factory('SourceSearchService', function($rootScope, $http, $q, $filter, ConfigService, SourceService) {
        var requestCanceler = $q.defer();
        function uniqueSources(sources) {
            var temp = [];
            var linkIndex = [];
            angular.forEach(sources, function (i) {
                if (linkIndex.indexOf(i.link) == -1) {
                    linkIndex.push(i.link);
                    temp.push(i)
                }
            });
            return temp;
        }
        return {
            grab: function (url, cancelPromise) {
                return $http.get(
                    ConfigService.CORE_API_HOST + '/api/v1/scanner/grab/?url=' + url,
                    {timeout: cancelPromise}
                ).then(function (response) {
                    return response.data;
                });
            },
            search: function (query, cancelPromise) {
                return $http.get(
                    ConfigService.CORE_API_HOST + '/api/v1/scanner/search/?query=' + query,
                    {timeout: cancelPromise}
                ).then(function (response) {
                    return response.data;
                });
            },
            _search: function (sourceQuery) {
                return SourceService.query().$promise.then(function (sources) {
                    if (!sourceQuery) return;
                    requestCanceler.resolve();
                    console.log(sources)
                    var resultSources = $filter('filter')(sources, {$: sourceQuery});

                    // online search
                    requestCanceler = $q.defer();
                    return $http.get(
                        ConfigService.CORE_API_HOST + '/api/v1/scanner/grab/?url=' + sourceQuery,
                        {timeout: requestCanceler.promise}
                    ).then(function (response) {
                        var dataArray = response.data;
                        console.log(dataArray)
                        if (!Array.isArray(response.data)) {
                            dataArray = [response.data];
                        }
                        angular.forEach(dataArray, function (data) {
                            if (data.type == 'site') {
                                var sources = [];
                                angular.forEach(data.rss, function (item) {
                                    sources.push({
                                        type: 'rss',
                                        name: item.title || data.name,
                                        icon: data.icon,
                                        url: data.url,
                                        link: item.link
                                    })
                                });
                                resultSources = resultSources.concat(sources);
                            } else {
                                resultSources.push(data);
                            }
                        });
                        return uniqueSources(resultSources)
                    }).catch(function () {
                        return resultSources;
                    });
                });
            }
        }
    })
    .factory('SourceTagService', function($rootScope, $resource, ConfigService) {
        return $resource(ConfigService.CORE_API_HOST + '/api/v1/sources/tags/');
    })
    .factory('SubscriptionService', function($rootScope, $resource, ConfigService) {
        return $resource(
            ConfigService.CORE_API_HOST + '/api/v1/subscriptions/:subscriptionId/',
            {subscriptionId: '@id'},
            {update: {method: 'PUT', params: {subscriptionId: '@id'}}},
            {delete: {method: 'DELETE'}});
    })
    .factory('SubscriptionTagService', function($rootScope, $resource, ConfigService) {
        return $resource(ConfigService.CORE_API_HOST + '/api/v1/subscriptions/tags/');
    })

    //// NEWS SERVICE ////

    .factory('NewsService', function($rootScope, $resource, $interval, $localStorage, $filter, UserService, ConfigService,
                                     CollectionService, NotificationService) {
        var headerId = null;
        var initId = null;
        var namedCallbacks = {};
        var callbacks = [];
        // indicators
        $localStorage.readNewsIds = $localStorage.readNewsIds || [];
        var unreadNews = [];

        function preventCache(){
            return new Date().getTime();
        }
        var News = $resource(
            ConfigService.CORE_API_HOST + '/api/v1/news/:newsId',
            {newsId: '@id', page_size: ConfigService.defaultPageSize, '_': preventCache}
        );
        function updateUnreadNews() {
            unreadNews = [];
            News.query({page: 1, page_size: 1000}).$promise.then(function (data) {
                unreadNews = $filter('filter')(data, function (item) {
                    return $localStorage.readNewsIds.indexOf(item._id) == -1
                });
            });
        }

        updateUnreadNews();

        $interval(function () {
            var initParams = {};
            if (headerId) {
                initParams.id__gt = headerId;
                var news = News.query(initParams, function () {
                    if (news.length) {
                        headerId = news[0]._id;
                        // update unread
                        unreadNews = $filter('filter')(news, function (item) {
                            return $localStorage.readNewsIds.indexOf(item._id) == -1
                        }).concat(unreadNews);
                        // call callback functions
                        angular.forEach(callbacks, function (callback) {
                            callback(news);
                        });
                        angular.forEach(namedCallbacks, function (callback, name) {
                            callback(news);
                        });
                        $rootScope.$broadcast("newsUpdateEvent");
                    }
                });
            }
        }, configData.updateNewsInterval * 1000);

        return {
            query: function (params) {
                if (params.page && params.page > 1 && initId) params.id__lte = initId;
                return News.query(params).$promise.then(function (data) {
                    if (data && data.length) {
                        if (params.page && params.page == 1) {
                            initId = data[0]._id;
                            headerId = data[0]._id;
                        }
                    }
                    return data;
                });
            },
            onUpdate: function (callback, callbackId) {
                if (callbackId) {
                    namedCallbacks[callbackId] = callback;
                } else {
                    callbacks.push(callback);
                }
            },
            getCount: function () {
                return unreadNews.length;
            },
            getCountByCollection: function (collection) {
                return $filter('filter')(unreadNews, function (item) {
                    return collection.sources.indexOf(item.source.id) != -1
                }).length;
            },
            isUnread: function (news) {
                return !!$filter('filter')(unreadNews, function (item) {
                    return news._id == item._id;
                }).length;
            },
            markRead: function (news) {
                if ($localStorage.readNewsIds.indexOf(news._id) == -1) {
                    $localStorage.readNewsIds.splice(0, 0, news._id);
                    $localStorage.readNewsIds.slice(0, 1000);
                    unreadNews = $filter('filter')(unreadNews, function (item) {
                        return news._id != item._id;
                    });
                }
            },
            markReadAll: function (collection) {
                var temp = [];
                angular.forEach(unreadNews, function (item, index) {
                    if (collection && collection.sources.indexOf(item.source.id) != -1 || collection == undefined) {
                        $localStorage.readNewsIds.splice(0, 0, item._id);
                    } else {
                        temp.push(item);
                    }
                });
                $localStorage.readNewsIds.slice(0, 1000);
                unreadNews = temp;
            }
        }
    })
    .factory('CollectionService', function($rootScope, $resource, ConfigService) {
        return $resource(
            ConfigService.CORE_API_HOST + '/api/v1/collections/:collectionId/',
            {collectionId: '@id'},
            {
                delete: {method: 'DELETE', params: {collectionId: '@id'}},
                update: {method: 'PUT', params: {collectionId: '@id'}}
            }
        );
    })
    .factory('SidebarService', function ($rootScope, $mdSidenav) {
        return {
            toggle: function () {
                $mdSidenav('left').toggle();
            },
            close: function () {
                $mdSidenav('left').close();
            },
            rightToggle: function () {
                $mdSidenav('right').toggle();
            }
        }
    })
    .factory('NotificationService', function ($rootScope, $localStorage, $q, $interval, $window, CollectionService) {
        return {
            isAvailable: !!$window.Notification,
            getPermission: function () {
                return $window.Notification.permission;
            },
            requestPermission: function () {
                return $q(function (resolve, reject) {
                    if (!$window.Notification) return reject();
                    $window.Notification.requestPermission(function (permission) {
                        $window.Notification.permission = $window.Notification.permission || permission;
                        resolve($window.Notification.permission);
                    })
                })
            },
            notify: function (title, options, url) {
                return $q(function (resolve, reject) {
                    if (!$window.Notification || $window.Notification.permission == 'denied') {
                        return reject();
                    } else {
                        var n = new $window.Notification(title, options);
                        if (url) {
                            n.onclick = function () {
                                $window.open(url, '_blank');
                            }
                        }
                        resolve();
                    }
                })
            },
            notifyNews: function (news) {
                var service = this;
                if (Array.isArray(news)) {
                    angular.forEach(news, function (item) {
                        var title = item.subscription && item.subscription.name || item.source.name;
                        var text = item.title || item.shortText || item.text;
                        var icon = item.source.icon;
                        var url = item.href;
                        service.notify(title, {tag: 'news-' + item._id, body: text, icon: icon}, url);
                    });
                } else {
                    var title = news.subscription && news.subscription.name || news.source.name;
                    var text = news.title || news.shortText || news.text;
                    var icon = news.source.icon;
                    var url = news.href;
                    service.notify(title, {tag: 'news-' + news._id, body: text, icon: icon}, url);
                }
            },
            getSettings: function () {
                return $q(function (resolve, reject) {
                    resolve($localStorage.notificationSettings || {});
                })
            },
            updateSettings: function (settings) {
                return $q(function (resolve, reject) {
                    $localStorage.notificationSettings = settings;
                    resolve($localStorage.notificationSettings);
                })
            }
        }
    })
    .factory('TimezonesService', function ($rootScope, $http) {
        return {
            getTimezones: function () {
                return $http.get('/static/json/timezones.json', {cache: true}).then(function (response) {
                    var timezones = [];
                    angular.forEach(response.data, function (item) {
                        if (item.offset % 1 === 0) {
                            var value;
                            if (item.offset >= 10 || item.offset <= -10) {
                                value = item.offset + '00';
                            } else if (item.offset >= 0) {
                                value = '+0' + item.offset + '00';
                            } else if (item.offset < 0) {
                                value = '-0' + Math.abs(item.offset) + '00';
                            }
                            timezones.push({
                                name: item.text,
                                value: value
                            })
                        }
                    });
                    return timezones;
                });
            },
            getTimezoneByName: function (name) {
                return this.getTimezones().then(function (data) {
                    var result = null;
                    angular.forEach(data, function (item) {
                        if (item.name == name) result = item.value;
                    });
                    return result;
                })
            }
        }
    })
    .factory('TranslationsService', function ($rootScope, $resource, ConfigService) {
        var resource = $resource(
            ConfigService.CORE_API_HOST + '/api/v1/translations/:translationId/',
            {translationId: '@id'}, {query: {method: 'GET', cache: true, isArray: false}}
        );
        var translations = resource.query();
        return {
            query: function (params) {
                return resource.query(params);
            },
            trans: function (slug, lang) {
                lang = lang || $rootScope.lang || 'en';
                try {
                    return translations[slug]['text_' + lang];
                } catch (error) {
                    return slug;
                }
            }
        }
    });